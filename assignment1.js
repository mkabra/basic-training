// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest(array) {
   var max = Math.max.apply(null, array);
  array.splice(array.indexOf(max),1)
  return Math.max.apply(null,array)
}

// Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
function calculateFrequency(string) {
  var pattern = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
var counter = {};
if(pattern.test(string)){
  
  var sorted = string.replace(/[^a-zA-Z ]/g, "").split('').sort().join('');
  var low = sorted.toLowerCase();
  var big = sorted.toUpperCase();
  var result = "";
    for(var i=0; i<low.length; ++i) {
      if((low[i] != big[i]) || (low[i].trim() === '')) {
        result += sorted[i];
      }
    }  
   for (var i=0; i<result.length;i++) {
        var char = result.charAt(i);
        counter[char] ? counter[char]++ : counter[char] = 1;
    }

} 
else {
   for (var i=0; i<string.length;i++) {
        var char = string.charAt(i);
        if(char != " ")
			counter[char] ? counter[char]++ : counter[char] = 1;
    }
} 
return counter;
}

// Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
function flatten(unflatObject) {
  function iter(part, keys) {
        Object.keys(part).forEach(function (k) {
         
        var allKeys = keys.concat(k);
            flat[keys.concat(k).join('.')] = part[k];
            if (part[k] !== null && typeof part[k] === 'object') {
                iter(part[k], keys.concat(k));
            }
        });
    }

    function getKeyByValue(object, value) {
      return Object.keys(object).find(key => object[key] === value);
    }

    Object.prototype.push = function( key, value ){
       this[ key ] = value;
       return this;
    }

    var flat = {};
    iter(unflatObject, []);

    var keys = Object.keys(flat);
    var f = {};
    for (var i = 0; i < keys.length; i++) {
        var val = flat[keys[i]];
            if(typeof val !== 'object'){
                f.push((getKeyByValue(flat,val)),val);
    }}

    return f;
}

// Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format
function unflatten(flatObject) {
  // Write your code here
}
